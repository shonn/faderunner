#ifndef ColorConvertor_h
#define ColorConvertor_h
#include "Colors.h"

RgbColor HsvToRgb(HsvColor);

HsvColor RgbToHsv(RgbColor);

#endif
