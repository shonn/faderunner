#ifndef Animation_h
#define Animation_h
#include <avr/pgmspace.h>
#include "TransitionType.h"

// female - greenish
#define FIGURE1H 110
#define FIGURE1S 200
// male - orange
#define FIGURE2H 20
#define FIGURE2S 240
// center - purple
#define FIGURE3H 220
#define FIGURE3S 240

// [groupIndex][segmentIndex][state][animationStep][h,s,v,steps,TransitionType]
prog_uchar aniHSVSegments_pulse[3][4][3][6][5] PROGMEM = {
    // segment groups
    {
        // segments
        {
            // states
            {
                // animations
                // h,s,v,steps, type
                {FIGURE1H,FIGURE1S,255, 255, LINEAR},
                {FIGURE1H,FIGURE1S,0, 255, LINEAR},
                {FIGURE1H,FIGURE1S,255, 255, LINEAR},
                {FIGURE1H,FIGURE1S,0, 255, LINEAR},
                {FIGURE1H,FIGURE1S,255, 255, LINEAR},
                {FIGURE1H,FIGURE1S,0, 255, LINEAR}
            },
            {
                // animations
                // h,s,v,steps, type
                {FIGURE1H,FIGURE1S,255, 255, LINEAR},
                {FIGURE1H,FIGURE1S,0, 255, LINEAR},
                {FIGURE1H,FIGURE1S,255, 255, LINEAR},
                {FIGURE1H,FIGURE1S,0, 255, LINEAR},
                {FIGURE1H,FIGURE1S,255, 255, LINEAR},
                {FIGURE1H,FIGURE1S,0, 255, LINEAR}
            },
            {
                // animations
                // h,s,v,steps, type
                {FIGURE1H,FIGURE1S,255, 255, LINEAR},
                {FIGURE1H,FIGURE1S,0, 255, LINEAR},
                {FIGURE1H,FIGURE1S,255, 255, LINEAR},
                {FIGURE1H,FIGURE1S,0, 255, LINEAR},
                {FIGURE1H,FIGURE1S,255, 255, LINEAR},
                {FIGURE1H,FIGURE1S,0, 255, LINEAR}
            }
        },
        {
            // states
            {
                // animations
                // h,s,v,steps
                {FIGURE1H,FIGURE1S,0, 200, LINEAR},
                {FIGURE1H,FIGURE1S,255, 255, LINEAR},
                {FIGURE1H,FIGURE1S,0, 200, LINEAR},
                {FIGURE1H,FIGURE1S,255, 255, LINEAR},
                {FIGURE1H,FIGURE1S,0, 200, LINEAR},
                {FIGURE1H,FIGURE1S,255, 255, LINEAR}
            },
            {
                // animations
                // h,s,v,steps
                {FIGURE1H,FIGURE1S,0, 200, LINEAR},
                {FIGURE1H,FIGURE1S,255, 255, LINEAR},
                {FIGURE1H,FIGURE1S,0, 200, LINEAR},
                {FIGURE1H,FIGURE1S,255, 255, LINEAR},
                {FIGURE1H,FIGURE1S,0, 200, LINEAR},
                {FIGURE1H,FIGURE1S,255, 255, LINEAR}
            },
            {
                // animations
                // h,s,v,steps
                {FIGURE1H,FIGURE1S,0, 200, LINEAR},
                {FIGURE1H,FIGURE1S,255, 255, LINEAR},
                {FIGURE1H,FIGURE1S,0, 200, LINEAR},
                {FIGURE1H,FIGURE1S,255, 255, LINEAR},
                {FIGURE1H,FIGURE1S,0, 200, LINEAR},
                {FIGURE1H,FIGURE1S,255, 255, LINEAR}
            }
        },
        {
            // states
            {
                // animations
                // h,s,v,steps, type
                {FIGURE1H,FIGURE1S,255, 255, LINEAR},
                {FIGURE1H,FIGURE1S,0, 100, LINEAR},
                {FIGURE1H,FIGURE1S,255, 255, LINEAR},
                {FIGURE1H,FIGURE1S,0, 255, LINEAR},
                {FIGURE1H,FIGURE1S,255, 255, LINEAR},
                {FIGURE1H,FIGURE1S,0, 100, LINEAR}
            },
            {
                // animations
                // h,s,v,steps, type
                {FIGURE1H,FIGURE1S,255, 255, LINEAR},
                {FIGURE1H,FIGURE1S,0, 100, LINEAR},
                {FIGURE1H,FIGURE1S,255, 255, LINEAR},
                {FIGURE1H,FIGURE1S,0, 255, LINEAR},
                {FIGURE1H,FIGURE1S,255, 255, LINEAR},
                {FIGURE1H,FIGURE1S,0, 100, LINEAR}
            },
            {
                // animations
                // h,s,v,steps, type
                {FIGURE1H,FIGURE1S,255, 255, LINEAR},
                {FIGURE1H,FIGURE1S,0, 100, LINEAR},
                {FIGURE1H,FIGURE1S,255, 255, LINEAR},
                {FIGURE1H,FIGURE1S,0, 255, LINEAR},
                {FIGURE1H,FIGURE1S,255, 255, LINEAR},
                {FIGURE1H,FIGURE1S,0, 100, LINEAR}
            }
        },
        {
            // states
            {
                // animations
                // h,s,v,steps
                {FIGURE1H,FIGURE1S,0, 255, LINEAR},
                {FIGURE1H,FIGURE1S,255, 200, LINEAR},
                {FIGURE1H,FIGURE1S,0, 255, LINEAR},
                {FIGURE1H,FIGURE1S,255, 200, LINEAR},
                {FIGURE1H,FIGURE1S,0, 255, LINEAR},
                {FIGURE1H,FIGURE1S,255, 255, LINEAR}
            },
            {
                // animations
                // h,s,v,steps
                {FIGURE1H,FIGURE1S,0, 255, LINEAR},
                {FIGURE1H,FIGURE1S,255, 200, LINEAR},
                {FIGURE1H,FIGURE1S,0, 255, LINEAR},
                {FIGURE1H,FIGURE1S,255, 200, LINEAR},
                {FIGURE1H,FIGURE1S,0, 255, LINEAR},
                {FIGURE1H,FIGURE1S,255, 255, LINEAR}
            },
            {
                // animations
                // h,s,v,steps
                {FIGURE1H,FIGURE1S,0, 255, LINEAR},
                {FIGURE1H,FIGURE1S,255, 200, LINEAR},
                {FIGURE1H,FIGURE1S,0, 255, LINEAR},
                {FIGURE1H,FIGURE1S,255, 200, LINEAR},
                {FIGURE1H,FIGURE1S,0, 255, LINEAR},
                {FIGURE1H,FIGURE1S,255, 255, LINEAR}
            }
        }
    },
    {
        // segments
        {
            // states
            {
                // animations
                // h,s,v,steps
                {FIGURE2H,FIGURE2S,255, 254, LINEAR},
                {FIGURE2H,FIGURE2S,0, 255, LINEAR},
                {FIGURE2H,FIGURE2S,255, 255, LINEAR},
                {FIGURE2H,FIGURE2S,0, 255, LINEAR},
                {FIGURE2H,FIGURE2S,255, 255, LINEAR},
                {FIGURE2H,FIGURE2S,0, 255, LINEAR},
            },
            {
                // animations
                // h,s,v,steps
                {FIGURE2H,FIGURE2S,255, 255, LINEAR},
                {FIGURE2H,FIGURE2S,0, 255, LINEAR},
                {FIGURE2H,FIGURE2S,255, 255, LINEAR},
                {FIGURE2H,FIGURE2S,0, 255, LINEAR},
                {FIGURE2H,FIGURE2S,255, 255, LINEAR},
                {FIGURE2H,FIGURE2S,0, 255, LINEAR},
            },
            {
                // animations
                // h,s,v,steps
                {FIGURE2H,FIGURE2S,255, 255, LINEAR},
                {FIGURE2H,FIGURE2S,0, 255, LINEAR},
                {FIGURE2H,FIGURE2S,255, 255, LINEAR},
                {FIGURE2H,FIGURE2S,0, 255, LINEAR},
                {FIGURE2H,FIGURE2S,255, 255, LINEAR},
                {FIGURE2H,FIGURE2S,0, 255, LINEAR},
            }
        },
        {
            // states
            {
                // animations
                // h,s,v,steps
                {FIGURE2H,FIGURE2S,0, 253, LINEAR},
                {FIGURE2H,FIGURE2S,255, 255, LINEAR},
                {FIGURE2H,FIGURE2S,0, 255, LINEAR},
                {FIGURE2H,FIGURE2S,255, 255, LINEAR},
                {FIGURE2H,FIGURE2S,0, 255, LINEAR},
                {FIGURE2H,FIGURE2S,255, 255, LINEAR}
            },
            {
                // animations
                // h,s,v,steps
                {FIGURE2H,FIGURE2S,0, 255, LINEAR},
                {FIGURE2H,FIGURE2S,255, 255, LINEAR},
                {FIGURE2H,FIGURE2S,0, 255, LINEAR},
                {FIGURE2H,FIGURE2S,255, 255, LINEAR},
                {FIGURE2H,FIGURE2S,0, 255, LINEAR},
                {FIGURE2H,FIGURE2S,255, 255, LINEAR}
            },
            {
                // animations
                // h,s,v,steps
                {FIGURE2H,FIGURE2S,0, 255, LINEAR},
                {FIGURE2H,FIGURE2S,255, 255, LINEAR},
                {FIGURE2H,FIGURE2S,0, 255, LINEAR},
                {FIGURE2H,FIGURE2S,255, 255, LINEAR},
                {FIGURE2H,FIGURE2S,0, 255, LINEAR},
                {FIGURE2H,FIGURE2S,255, 255, LINEAR}
            }
        },
        {
            // states
            {
                // animations
                // h,s,v,steps
                {FIGURE2H,FIGURE2S,255, 252, LINEAR},
                {FIGURE2H,FIGURE2S,0, 255, LINEAR},
                {FIGURE2H,FIGURE2S,255, 255, LINEAR},
                {FIGURE2H,FIGURE2S,0, 255, LINEAR},
                {FIGURE2H,FIGURE2S,255, 255, LINEAR},
                {FIGURE2H,FIGURE2S,0, 255, LINEAR},
            },
            {
                // animations
                // h,s,v,steps
                {FIGURE2H,FIGURE2S,255, 255, LINEAR},
                {FIGURE2H,FIGURE2S,0, 255, LINEAR},
                {FIGURE2H,FIGURE2S,255, 255, LINEAR},
                {FIGURE2H,FIGURE2S,0, 255, LINEAR},
                {FIGURE2H,FIGURE2S,255, 255, LINEAR},
                {FIGURE2H,FIGURE2S,0, 255, LINEAR},
            },
            {
                // animations
                // h,s,v,steps
                {FIGURE2H,FIGURE2S,255, 255, LINEAR},
                {FIGURE2H,FIGURE2S,0, 255, LINEAR},
                {FIGURE2H,FIGURE2S,255, 255, LINEAR},
                {FIGURE2H,FIGURE2S,0, 255, LINEAR},
                {FIGURE2H,FIGURE2S,255, 255, LINEAR},
                {FIGURE2H,FIGURE2S,0, 255, LINEAR},
            }
        },
        {
            // states
            {
                // animations
                // h,s,v,steps
                {FIGURE2H,FIGURE2S,0, 251, LINEAR},
                {FIGURE2H,FIGURE2S,255, 255, LINEAR},
                {FIGURE2H,FIGURE2S,0, 255, LINEAR},
                {FIGURE2H,FIGURE2S,255, 255, LINEAR},
                {FIGURE2H,FIGURE2S,0, 255, LINEAR},
                {FIGURE2H,FIGURE2S,255, 255, LINEAR}
            },
            {
                // animations
                // h,s,v,steps
                {FIGURE2H,FIGURE2S,0, 255, LINEAR},
                {FIGURE2H,FIGURE2S,255, 255, LINEAR},
                {FIGURE2H,FIGURE2S,0, 255, LINEAR},
                {FIGURE2H,FIGURE2S,255, 255, LINEAR},
                {FIGURE2H,FIGURE2S,0, 255, LINEAR},
                {FIGURE2H,FIGURE2S,255, 255, LINEAR}
            },
            {
                // animations
                // h,s,v,steps
                {FIGURE2H,FIGURE2S,0, 255, LINEAR},
                {FIGURE2H,FIGURE2S,255, 255, LINEAR},
                {FIGURE2H,FIGURE2S,0, 255, LINEAR},
                {FIGURE2H,FIGURE2S,255, 255, LINEAR},
                {FIGURE2H,FIGURE2S,0, 255, LINEAR},
                {FIGURE2H,FIGURE2S,255, 255, LINEAR}
            }
        }
    },
    {
        // segments
        {
            // states
            {
                // animations
                // h,s,v,steps
                {FIGURE3H,FIGURE3S,255, 100, LINEAR},
                {FIGURE3H,FIGURE3S,0, 255, LINEAR},
                {FIGURE3H,FIGURE3S,255, 255, LINEAR},
                {FIGURE3H,FIGURE3S,0, 255, LINEAR},
                {FIGURE3H,FIGURE3S,255, 255, LINEAR},
                {FIGURE3H,FIGURE3S,0, 255, LINEAR},
            },
            {
                // animations
                // h,s,v,steps
                {FIGURE3H,FIGURE3S,255, 255, LINEAR},
                {FIGURE3H,FIGURE3S,0, 255, LINEAR},
                {FIGURE3H,FIGURE3S,255, 255, LINEAR},
                {FIGURE3H,FIGURE3S,0, 255, LINEAR},
                {FIGURE3H,FIGURE3S,255, 255, LINEAR},
                {FIGURE3H,FIGURE3S,0, 255, LINEAR},
            },
            {
                // animations
                // h,s,v,steps
                {FIGURE3H,FIGURE3S,255, 255, LINEAR},
                {FIGURE3H,FIGURE3S,0, 255, LINEAR},
                {FIGURE3H,FIGURE3S,255, 255, LINEAR},
                {FIGURE3H,FIGURE3S,0, 255, LINEAR},
                {FIGURE3H,FIGURE3S,255, 255, LINEAR},
                {FIGURE3H,FIGURE3S,0, 255, LINEAR},
            }
        },
        {
            // states
            {
                // animations
                // h,s,v,steps
                {FIGURE3H,FIGURE3S,0, 75, LINEAR},
                {FIGURE3H,FIGURE3S,255, 255, LINEAR},
                {FIGURE3H,FIGURE3S,0, 255, LINEAR},
                {FIGURE3H,FIGURE3S,255, 255, LINEAR},
                {FIGURE3H,FIGURE3S,0, 255, LINEAR},
                {FIGURE3H,FIGURE3S,255, 255, LINEAR}
            },
            {
                // animations
                // h,s,v,steps
                {FIGURE3H,FIGURE3S,0, 255, LINEAR},
                {FIGURE3H,FIGURE3S,255, 255, LINEAR},
                {FIGURE3H,FIGURE3S,0, 255, LINEAR},
                {FIGURE3H,FIGURE3S,255, 255, LINEAR},
                {FIGURE3H,FIGURE3S,0, 255, LINEAR},
                {FIGURE3H,FIGURE3S,255, 255, LINEAR}
            },
            {
                // animations
                // h,s,v,steps
                {FIGURE3H,FIGURE3S,0, 255, LINEAR},
                {FIGURE3H,FIGURE3S,255, 255, LINEAR},
                {FIGURE3H,FIGURE3S,0, 255, LINEAR},
                {FIGURE3H,FIGURE3S,255, 255, LINEAR},
                {FIGURE3H,FIGURE3S,0, 255, LINEAR},
                {FIGURE3H,FIGURE3S,255, 255, LINEAR}
            }
        },
        {
            // states
            {
                // animations
                // h,s,v,steps
                {FIGURE3H,FIGURE3S,255, 125, LINEAR},
                {FIGURE3H,FIGURE3S,0, 255, LINEAR},
                {FIGURE3H,FIGURE3S,255, 255, LINEAR},
                {FIGURE3H,FIGURE3S,0, 255, LINEAR},
                {FIGURE3H,FIGURE3S,255, 255, LINEAR},
                {FIGURE3H,FIGURE3S,0, 255, LINEAR},
            },
            {
                // animations
                // h,s,v,steps
                {FIGURE3H,FIGURE3S,255, 255, LINEAR},
                {FIGURE3H,FIGURE3S,0, 255, LINEAR},
                {FIGURE3H,FIGURE3S,255, 255, LINEAR},
                {FIGURE3H,FIGURE3S,0, 255, LINEAR},
                {FIGURE3H,FIGURE3S,255, 255, LINEAR},
                {FIGURE3H,FIGURE3S,0, 255, LINEAR},
            },
            {
                // animations
                // h,s,v,steps
                {FIGURE3H,FIGURE3S,255, 255, LINEAR},
                {FIGURE3H,FIGURE3S,0, 255, LINEAR},
                {FIGURE3H,FIGURE3S,255, 255, LINEAR},
                {FIGURE3H,FIGURE3S,0, 255, LINEAR},
                {FIGURE3H,FIGURE3S,255, 255, LINEAR},
                {FIGURE3H,FIGURE3S,0, 255, LINEAR},
            }
        },
        {
            // states
            {
                // animations
                // h,s,v,steps
                {FIGURE3H,FIGURE3S,0, 150, LINEAR},
                {FIGURE3H,FIGURE3S,255, 255, LINEAR},
                {FIGURE3H,FIGURE3S,0, 255, LINEAR},
                {FIGURE3H,FIGURE3S,255, 255, LINEAR},
                {FIGURE3H,FIGURE3S,0, 255, LINEAR},
                {FIGURE3H,FIGURE3S,255, 255, LINEAR}
            },
            {
                // animations
                // h,s,v,steps
                {FIGURE3H,FIGURE3S,0, 255, LINEAR},
                {FIGURE3H,FIGURE3S,255, 255, LINEAR},
                {FIGURE3H,FIGURE3S,0, 255, LINEAR},
                {FIGURE3H,FIGURE3S,255, 255, LINEAR},
                {FIGURE3H,FIGURE3S,0, 255, LINEAR},
                {FIGURE3H,FIGURE3S,255, 255, LINEAR}
            },
            {
                // animations
                // h,s,v,steps
                {FIGURE3H,FIGURE3S,0, 255, LINEAR},
                {FIGURE3H,FIGURE3S,255, 255, LINEAR},
                {FIGURE3H,FIGURE3S,0, 255, LINEAR},
                {FIGURE3H,FIGURE3S,255, 255, LINEAR},
                {FIGURE3H,FIGURE3S,0, 255, LINEAR},
                {FIGURE3H,FIGURE3S,255, 255, LINEAR}
            }
        }
    }
};


#endif Animation_h

// prog_uchar aniRGB[][3] PROGMEM = {
//     {
//         0, 0, 0
//     },{
//         30, 120, 180
//     },{
//         80, 200, 200
//     },{
//         255, 255, 255
//     },{
//         200, 255, 255
//     },{
//         240, 200, 200
//     },{
//         100, 150, 150
//     }
// };
