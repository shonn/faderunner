#include <Tlc5940.h>
#include <avr/pgmspace.h>
#include "Colors.h"
#include "Animation.h"
#include "ColorConvertor.h"
#include "Fades.h"
#include "Segment.h"

// TODO: color range is limited to 255 instead of 4096
// 		 this aids in testing (ow, my eyes) but will need
//		 to be fixed before release
int pin_r = 0;
int pin_g = 1;
int pin_b = 2;

#define GROUPS 3
#define SEGMENTS 4
#define ANIMATIONSTEPS 6
#define STATES 3

Segment segments [GROUPS][SEGMENTS];

int beacon = 0;

RgbColor rgb;
HsvColor hsv;
Segment *segment;
unsigned int groupIndex;
unsigned int segmentIndex;

void getNextAnimation (unsigned int groupIndex, unsigned int segmentIndex, Segment *segment) {
    // archive current step from current value in case the animation hasn't completed
    segment->prevHsv.h = segment->hsv.h;
    segment->prevHsv.s = segment->hsv.s;
    segment->prevHsv.v = segment->hsv.v;

    // reset current step
    segment->currentStep = 0;

    segment->nextHsv.h = pgm_read_byte(&(aniHSVSegments_pulse[groupIndex][segmentIndex][segment->state][segment->animationStep][0]));
    segment->nextHsv.s = pgm_read_byte(&(aniHSVSegments_pulse[groupIndex][segmentIndex][segment->state][segment->animationStep][1]));
    segment->nextHsv.v = pgm_read_byte(&(aniHSVSegments_pulse[groupIndex][segmentIndex][segment->state][segment->animationStep][2]));
    segment->steps = pgm_read_byte(&(aniHSVSegments_pulse[groupIndex][segmentIndex][segment->state][segment->animationStep][3]));
    segment->transitionType = (TransitionType)pgm_read_byte(&(aniHSVSegments_pulse[groupIndex][segmentIndex][segment->state][segment->animationStep][4]));

    // increment animation step
    segment->animationStep++;
    if (segment->animationStep >= ANIMATIONSTEPS)
    {
        segment->animationStep = 0;
    }
}

void setup()
{
    Tlc.init();

    // set defaults
    for (groupIndex = 0; groupIndex < GROUPS; groupIndex++)
    {
        for (segmentIndex = 0; segmentIndex < SEGMENTS; segmentIndex++)
        {
            segment = &segments[groupIndex][segmentIndex];

            // set everything to 0 with a step of 0
            // on the first run it will see that step and curr are 0 and advance to the next ani
            // the first ani has a 0 steps so it will show 0 for a step then transition to the next step

            segment->steps = 0;
            segment->currentStep = 0;
            segment->animationStep = 0;
            segment->state = 0;

            // TODO: load in first animation instead of default color
            segment->hsv.h = 200;
            segment->hsv.s = 255;
            segment->hsv.v = 255;
        }
    }
}

void loop()
{
    // TODO: check buttons for state

    // loop through groups/segments
    for (groupIndex = 0; groupIndex < GROUPS; groupIndex++)
    {
        for (segmentIndex = 0; segmentIndex < SEGMENTS; segmentIndex++)
        {
            segment = &segments[groupIndex][segmentIndex];

            if (segment->currentStep >= segment->steps)
            {
                getNextAnimation(groupIndex, segmentIndex, segment);
            }

            hsv = linearFade(segment);
            // TODO: mode these inside linearFade
            segment->hsv.h = hsv.h;
            segment->hsv.s = hsv.s;
            segment->hsv.v = hsv.v;

            rgb = HsvToRgb(hsv);

            Tlc.set((groupIndex*16) + (segmentIndex*3) + pin_r, expMap(rgb.r)); // map(r, 0, 255, 0, max_value));
            Tlc.set((groupIndex*16) + (segmentIndex*3) + pin_g, expMap(rgb.g)); // map(g, 0, 255, 0, max_value));
            Tlc.set((groupIndex*16) + (segmentIndex*3) + pin_b, expMap(rgb.b)); // map(b, 0, 255, 0, max_value));

            segment->currentStep++;
        }
    }

    // Tlc.set(13, 1);
    Tlc.update();
    delay(10);//4);
}
