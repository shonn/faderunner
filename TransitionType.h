#ifndef TransitionType_h
#define TransitionType_h
typedef enum {
    LINEAR,
    LOG,
    INVLOG,
    IMMEDIATE,
    EXP,
    INVEXP,
    RND
} TransitionType;

#endif TransitionType_h
