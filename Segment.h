#ifndef Segment_h
#define Segment_h
#include "Colors.h"
#include "TransitionType.h"

typedef struct Segment
{
    // color value that the segment is transitioning from
    HsvColor prevHsv;
    // color value that the segment is transitioning to
    HsvColor nextHsv;
    // transitioned color value
    HsvColor hsv;
    // transition type
    TransitionType transitionType;
    // total steps for this transition
    unsigned int steps;
    // current transition step
    unsigned int currentStep;
    // current step in the animation
    unsigned int animationStep;
    // current state of the segment
    unsigned char state;
} Segment;

#endif Segment_h
