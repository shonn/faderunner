#ifndef Fades_h
#define Fades_h
#include "Colors.h"
#include "Segment.h"

HsvColor linearFade(Segment *segment);
unsigned char getLinearFade(unsigned char, unsigned char, unsigned char, unsigned char);
unsigned int expMap(unsigned char);

#endif


