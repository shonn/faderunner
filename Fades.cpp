#include <Math.h>
#include "Fades.h"
#include "Segment.h"


HsvColor linearFade(Segment *segment) {
    HsvColor hsv;
    hsv.h = getLinearFade(segment->currentStep, segment->steps, segment->prevHsv.h, segment->nextHsv.h);
    hsv.s = getLinearFade(segment->currentStep, segment->steps, segment->prevHsv.s, segment->nextHsv.s);
    hsv.v = getLinearFade(segment->currentStep, segment->steps, segment->prevHsv.v, segment->nextHsv.v);
    return hsv;
}

unsigned char getLinearFade(unsigned char currentStep, unsigned char steps, unsigned char prevValue, unsigned char nextValue) {
    return (unsigned char)( ( (long)(nextValue - prevValue) * (long)currentStep) / steps) + prevValue;
}

unsigned int expMap(unsigned char value) {
    // hard coding to 0-4095 range

    // http://stackoverflow.com/questions/846221/logarithmic-slider
    // scale = Math.log(4095)/255 = 0.03261773331877321
    // exp(scale*position)
    double const scale = 0.03261773331877321;
    return exp(scale * (double)value) - 1; // -1 is a tweak to get it to 0
}
